call plug#begin()
" Appearance
Plug 'itchyny/lightline.vim'
Plug 'ryanoasis/vim-devicons'
Plug 'jacoborus/tender.vim'
Plug 'vim-scripts/chlordane.vim'
Plug 'xiyaowong/nvim-transparent'

" Utilities
Plug 'sheerun/vim-polyglot'
Plug 'windwp/nvim-autopairs'
Plug 'alvan/vim-closetag'
Plug 'ms-jpq/coq_nvim'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'ap/vim-css-color'
Plug 'preservim/nerdtree'
Plug 'lewis6991/impatient.nvim'
Plug 'RRethy/nvim-treesitter-endwise'
Plug 'neovim/nvim-lspconfig'
Plug 'jose-elias-alvarez/null-ls.nvim'

" Completion / linters / formatters
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'plasticboy/vim-markdown'
Plug 'Alloyed/lua-lsp'

" Git
Plug 'airblade/vim-gitgutter'
call plug#end()

set noshowmode
set background=dark
set clipboard+=unnamedplus
set completeopt=noinsert,menuone,noselect
set cursorline
set hidden
set inccommand=split
set mouse=a
set number
set relativenumber
set splitbelow splitright
set title
set ttimeoutlen=0
set wildmenu

set expandtab
set shiftwidth=2
set tabstop=2

highlight EndOfBuffer ctermfg=black ctermbg=black

inoremap <silent><expr> <S-Tab> coc#pum#insert()

filetype plugin indent on
syntax on
set t_Co=256

let term_program=$TERM_PROGRAM

if has("termguicolors")
		set termguicolors
else
	if $TERM !=? 'xterm-256color'
		set termguicolors
	endif
endif

syntax enable
colorscheme tender

let g:lightline = {
	\	'colorscheme': 'seoul256',
	\	}

let &t_ZH="\e[3m"
let &t_ZR="\e[23m"

let g:netrw_banner=0
let g:netrw_liststyle=0
let g:netrw_browse_split=4
let g:netrw_altv=1
let g:netrw_winsize=25
let g:netrw_keepdir=0
let g:netrw_localcopydircmd='cp -r'

function! CreateInPreview()
	let l:filename = input('filename: ')
	execute 'silent !touch ' . b:netrw_curdir.'/'.l:filename
	redraw!
endfunction

function! Netrw_mappings()
	noremap <buffer>% :call CreateInPreview()<cr>
endfunction

augroup auto_commands
	autocmd filetype netrw call Netrw_mappings()
augroup END

let NERDTreeShowHidden=1
nnoremap fs :NERDTreeToggle<CR>

command! -nargs=0 Prettier :call CocAction('runCommand', 'prettier.formatFile')

autocmd FileType html set omnifunc=htmlcomplete#CompleteTags

lua << EOF
local npairs = require("nvim-autopairs")
npairs.setup({
	map_cr = true,
	check_ts = true,
})
npairs.add_rules(require("nvim-autopairs.rules.endwise-lua"))

require("nvim-treesitter.configs").setup {
	autotag = {
		enable = true,
	},
	endwise = {
		enable = true,
	}
}

local coq = require("coq")

require("transparent").setup({
	enable = true,
	extra_groups = {
		"BufferLineTabClose",
		"BufferLineBufferSelected",
		"BufferLineFill",
		"BufferLineBackground",
		"BufferLineSeparator",
		"BufferLineIndicatorSelected",
	},
	exclude = {},
})

--lsp.bashls.setup()
--lsp.cssls.setup()
--lsp.html.setup()
--lsp.clangd.setup()
--lsp.jsonls.setup()
EOF
